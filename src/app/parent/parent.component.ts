import { AfterViewChecked, AfterViewInit, Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-parent',
    templateUrl: './parent.component.html',
    styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, AfterViewInit, AfterViewChecked {

    public data: any = {a: 'a'};
    public message: string = 'it is ' + new Date().toISOString() + '.';
    public items: Date[] = [];

    public ngAfterViewChecked(): void {
        // if any variable used by the template changes (i.e., the reference and the value itself changed),
        // it MAY throw the "...expression changed after was checked..." error
        // we are only supposed to change those variables until the ngOnInit

        // this.message = 'changed in wrong place';
        console.log(ParentComponent.name + ': ngAfterViewChecked');
    }

    public ngAfterViewInit(): void {
        // if any variable used by the template changes (i.e., the reference and the value itself changed),
        // it MAY throw the "...expression changed after was checked..." error
        // we are only supposed to change those variables until the ngOnInit

        // the below causes the error:
        // this.message = 'changed in wrong place';

        // however, the below don't (maybe because it doesn't compare object?):
        // this.data = { a: 'b', b: 'c', c: 'd' };
        console.log(ParentComponent.name + ': ngAfterViewInit');
    }

    public ngOnInit(): void {
        console.log(ParentComponent.name + ': ngOnInit');
    }

    public addItem(): void {
        this.items.push(new Date());
    }

    public refreshMessage(): void {
        this.message = 'it is ' + new Date().toISOString() + '.';
    }

    public setProperty(): void {
        this.data.b = 'b';
    }
}
