import { AfterViewChecked, AfterViewInit, Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'app-child',
    templateUrl: './child.component.html',
    styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, AfterViewInit, AfterViewChecked {

    @Input() public data: any = null;

    public ngAfterViewChecked(): void {
        console.log(ChildComponent.name + ': ngAfterViewChecked');
    }

    public ngAfterViewInit(): void {
        console.log(ChildComponent.name + ': ngAfterViewInit');
    }

    public ngOnInit(): void {
        console.log(ChildComponent.name + ': ngOnInit');
    }
}
